import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    home: MainScreeen(),
  ));
}

class HeroApp extends StatelessWidget {
  const HeroApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Transition Demo',
      home: MainScreeen(),
    );
  }
}

class MainScreeen extends StatelessWidget {
  const MainScreeen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Mian Screen'),
        ),
        body: GestureDetector(
          onTap: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return const DetailScreen();
            }));
          },
          child: Hero(
            child: Image.network(
                'https://pbs.twimg.com/media/E7U6VLxVoAQcU6-?format=jpg&name=360x360'),
            tag: 'imageHero',
          ),
        ));
  }
}

class DetailScreen extends StatelessWidget {
  const DetailScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context);
      },
      child: Center(
          child: Hero(
              child: Image.network(
                  'https://pbs.twimg.com/media/E7U6VLxVoAQcU6-?format=jpg&name=360x360'),
              tag: 'imageHero')),
    );
  }
}
